﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JBe.System.IO
{
    public partial class FileLineReader
    {
        private sealed class LineEnumerator : IEnumerable<String>, IDisposable
        {
            private readonly Options options;
            private readonly CancellationTokenSource cancelTokenSource;
            private readonly BlockingCollection<String> bufferedLines;

            private String line;

            internal LineEnumerator(Options options)
            {
                this.options = options;

                this.cancelTokenSource = new CancellationTokenSource();
                this.bufferedLines = new BlockingCollection<string>(options.BufferSize);
            }

            public void Dispose()
            {
                cancelTokenSource.Cancel();

                GC.SuppressFinalize(this);
            }

            public IEnumerator<string> GetEnumerator()
            {
                StartReadingFromFileAsync(cancelTokenSource.Token);

                String line;

                while (!bufferedLines.IsCompleted)
                {
                    try
                    {
                        if (!bufferedLines.TryTake(out line, -1, cancelTokenSource.Token)) { yield break; }
                    }
                    catch (OperationCanceledException)
                    { yield break; }

                    yield return line;
                }
            }


            private Task StartReadingFromFileAsync(CancellationToken cancelToken)
            {
                return Task.Factory.StartNew(() =>
                {
                    using (FileStream fs = File.OpenRead(options.FileName))
                    using (var reader = new StreamReader(fs, options.Encoding, false, options.IOBufferSize))
                    {
                        try
                        {
                            String line;
                            while (reader.Peek() != 0)
                            {
                                if (cancelToken.IsCancellationRequested)
                                { break; }

                                line = reader.ReadLine();
                                if (line == null)
                                { break; }

                                bufferedLines.Add(line, cancelToken);
                            }
                        }
                        catch (OperationCanceledException)
                        { }
                        finally
                        { bufferedLines.CompleteAdding(); }
                    }
                }, cancelToken);
            }

            global::System.Collections.IEnumerator global::System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            internal class Options
            {
                public String FileName;
                public int IOBufferSize;
                public Encoding Encoding;
                public int BufferSize;
            }
        }
    }
}
