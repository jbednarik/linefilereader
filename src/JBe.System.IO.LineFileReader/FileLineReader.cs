﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JBe.System.IO
{
    public sealed partial class FileLineReader
    {
        private readonly string filename;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileLineReader"/> class.
        /// </summary>
        /// <param name="fullFilename">The full filename.</param>
        public FileLineReader(String fullFilename)
        {
            this.filename = fullFilename;

            Encoding = Encoding.ASCII;
            BufferSize = 999999;
            IOReadBufferSize = 64 * 1024;
        }

        /// <summary>
        /// Gets or sets the encoding.
        /// </summary>
        /// <value>
        /// The encoding.
        /// </value>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Gets or sets the size of the buffer.
        /// </summary>
        /// <value>
        /// The size of the buffer.
        /// </value>
        public int BufferSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the io read buffer.
        /// </summary>
        /// <value>
        /// The size of the io read buffer.
        /// </value>
        public int IOReadBufferSize { get; set; }

        /// <summary>
        /// Enumerates the lines in the file.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> EnumerateLines()
        {
            var options = new FileLineReader.LineEnumerator.Options
            {
                BufferSize = BufferSize,
                Encoding = Encoding,
                FileName = filename,
                IOBufferSize = IOReadBufferSize
            };

            var lineEnumerator = new FileLineReader.LineEnumerator(options);

            try
            {
                foreach (var item in lineEnumerator)
                {
                    yield return item;
                }
            }
            finally
            {
                lineEnumerator.Dispose();
            }
        }
    }
}
