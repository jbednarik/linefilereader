﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            const string filename = @"C:\temp\AUDCAD_Ticks_2006.01.03_2013.06.30.csv";

            var fileLieReader = new JBe.System.IO.FileLineReader(filename);

            var sw = Stopwatch.StartNew();

            double count = 0;
            foreach (var item in fileLieReader.EnumerateLines())
            {
                if (count++ % 1000 == 0)
                {
                    Console.WriteLine(item);
                }
            }

            Console.WriteLine(sw.Elapsed);

            Console.WriteLine("Press ENTER to exit.");
            Console.ReadLine();
        }
    }
}
